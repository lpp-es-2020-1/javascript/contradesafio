import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';

import { UserModule } from './user/user.module';
import { User } from './user/user.entity';
import { UrlModule } from './url/url.module';
import { Url } from './url/url.entity';

@Module({
  imports: [
    UserModule,
    UrlModule,
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: './dados.sql',
      synchronize: true,
      entities: [User, Url],
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
