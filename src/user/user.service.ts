import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { User } from './user.entity';

import { InjectRepository } from '@nestjs/typeorm';
import { getRepository, Repository } from 'typeorm';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
  ) {}

  findAll(): Promise<User[]> {
    return this.usersRepository.find();
  }

  findOne(id: string): Promise<User> {
    return this.usersRepository.findOne(id);
  }

  async create(dto: User): Promise<User> {
    const { name, email, password } = dto;
    const qb = await getRepository(User)
      .createQueryBuilder('user')
      .where('user.email = :email', { email });

    const user = await qb.getOne();

    if (user) {
      const errors = { email: 'E-mail não disponível' };
      throw new HttpException(
        { message: 'Falha na validação de dados', errors },
        HttpStatus.BAD_REQUEST,
      );
    }

    const newUser = new User();
    newUser.name = name;
    newUser.email = email;
    newUser.password = password;

    return this.usersRepository.save(newUser);
  }

  async delete(id: number): Promise<any> {
    const user = await this.usersRepository.findOne({ id: id });

    if (user) {
      return await this.usersRepository.delete({ id: id });
    }
    const errors = { email: 'User inválido' };
    throw new HttpException(
      { message: 'Falha na validação de dados', errors },
      HttpStatus.BAD_REQUEST,
    );
  }
}
