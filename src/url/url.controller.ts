import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Res,
} from '@nestjs/common';
import { Response } from 'express';
import { UrlService } from './url.service';
import { Url } from './url.entity';

@Controller('urls')
export class UrlController {
  constructor(private readonly urlsService: UrlService) {}

  @Get()
  async getAll(): Promise<Url[]> {
    return this.urlsService.findAll();
  }

  @Get('access/:url')
  async accessUrl(@Param('url') url_encurtada: string, @Res() res: Response) {
    console.log(url_encurtada);
    const response = await this.urlsService.access(url_encurtada);
    if (response.url_to_redirect) {
      return res.redirect(response.url_to_redirect);
    }
  }

  @Post()
  async create(@Body('url') url: string): Promise<Url> {
    return this.urlsService.create(url);
  }

  @Delete(':id')
  async delete(@Param('id') id: string): Promise<Url> {
    return this.urlsService.delete(parseInt(id));
  }
}
