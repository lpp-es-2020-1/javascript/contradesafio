import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
// import * as bcrypt from 'bcryptjs';
import { authenticator } from 'otplib';

import { Url } from './url.entity';
import { UsersService } from '../user/user.service';

import { Repository } from 'typeorm';
import { HashAlgorithms, KeyEncodings } from '@otplib/core';

authenticator.options = {
  algorithm: HashAlgorithms.SHA256,
  encoding: KeyEncodings.UTF8,
  digits: 10,
};

@Injectable()
export class UrlService {
  private bcrypt: any;
  constructor(
    @InjectRepository(Url) private urlsRepository: Repository<Url>,
    private usersService: UsersService,
  ) {}

  findAll(): Promise<Url[]> {
    return this.urlsRepository.find();
  }

  findOne(params: any): Promise<Url | undefined> {
    return this.urlsRepository.findOne(params);
  }

  async create(url: string): Promise<any> {
    const newUrl = new Url();
    newUrl.url_original = url;
    newUrl.url_encurtada = await authenticator.generate(newUrl.url_original);
    newUrl.owner = await this.usersService.findOne('1');
    return await this.urlsRepository.save(newUrl);
  }

  async access(url: string): Promise<any> {
    const urlEncontrada = await this.urlsRepository.findOne({
      url_encurtada: url,
    });

    if (urlEncontrada) {
      return { url_to_redirect: urlEncontrada.url_original };
    } else {
      const errors = { email: 'Url inválida' };
      throw new HttpException(
        { message: 'Falha na validação de dados', errors },
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  async delete(id: number): Promise<any> {
    const user = await this.urlsRepository.findOne({ id: id });

    if (user) {
      return await this.urlsRepository.delete({ id: id });
    }
    const errors = { email: 'User inválido' };
    throw new HttpException(
      { message: 'Falha na validação de dados', errors },
      HttpStatus.BAD_REQUEST,
    );
  }
}
