import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from '../user/user.entity';

@Entity('url')
export class Url {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  url_encurtada: string;

  @Column()
  url_original: string;

  @ManyToOne(() => User, (user: User) => user.urls)
  owner: User;
}
